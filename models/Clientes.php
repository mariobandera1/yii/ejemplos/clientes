<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $telefono
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nombre', 'apellidos', 'telefono'], 'required'],
            [['id'], 'integer'],
            [['nombre'], 'string', 'max' => 200],
            [['apellidos'], 'string', 'max' => 300],
            [['telefono'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código Cliente',
            'nombre' => 'Nombre Cliente',
            'apellidos' => 'Apellidos del cliente',
            'telefono' => 'Telefono movil',
        ];
    }
}
